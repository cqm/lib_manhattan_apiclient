<?php

namespace CQM\Libraries\Manhattan\ApiClient;

use CQM\Libraries\Manhattan\ApiClient\Exceptions\BadResponseException;
use CQM\Libraries\Manhattan\ApiClient\Exceptions\CurlException;
use CQM\Libraries\Manhattan\ApiClient\Exceptions\ErrorResponseException;
use CQM\Libraries\Security\Signature;

class ApiClient
{

    /**
     * @var array
     */
    private $config = array(
        'endpoint' => '',
        'timeout' => 30,
        'consumer_key' => '',
        'consumer_secret' => '',
        'verify_ssl' => true,
        'ssl_certs_bundle' => __DIR__ . '/../assets/cacert.pem',
    );

    /**
     * Instantiates the client.
     *
     * @param array $config API Client configuration.
     */
    function __construct(array $config)
    {
        $this->setConfig($config);
    }

    private function setConfig(array $config)
    {
        $config = array_replace($this->config, $config);

        $config['endpoint'] = rtrim($config['endpoint'], '/');

        $this->config = $config;
    }

    /**
     * Performs a GET request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function get($resource, array $params = [])
    {
        return $this->doRequest('GET', $resource, $params);
    }

    /**
     * Performs a POST request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function post($resource, array $params = [])
    {
        return $this->doRequest('POST', $resource, $params);
    }

    /**
     * Performs a PUT request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function put($resource, array $params = [])
    {
        return $this->doRequest('PUT', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function patch($resource, array $params = [])
    {
        return $this->doRequest('PATCH', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function link($resource, array $params = [])
    {
        return $this->doRequest('LINK', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function unlink($resource, array $params = [])
    {
        return $this->doRequest('UNLINK', $resource, $params);
    }

    /**
     * Performs a request
     *
     * @param string $method HTTP method (GET, POST, PUT, PATCH, LINK, UNLINK)
     * @param string $resource Resource path
     * @param array $params Request params
     * @return Response Response
     */
    protected function doRequest($method, $resource, array $params)
    {
        list($resource, $params) = $this->processResourceAndParams($resource, $params);

        $try = 0;
        do {
            $curl = $this->curlInit($resource, $params, $method);
            $result = $this->curlExec($curl);
            $response = $this->createResponse($result);
        } while(++$try < 3 && $response->isError() && $response['code'] === 4005);
        // ^ Retry the request up to 3 times in case of duplicated request error

        if ($response->isError()) {
            throw new ErrorResponseException($response);
        }

        return $response;
    }

    /**
     * Replaces variables in the resource string
     *
     * @param string $resource
     * @param array $params
     * @return array
     */
    protected function processResourceAndParams($resource, array $params)
    {
        $resource = trim($resource, '/');

        foreach ($params as $key => $value) {
            if (!is_scalar($value)) {
                continue;
            }

            $resource = str_replace('{'. $key .'}', urlencode($value), $resource, $replaced);

            if ($replaced) {
                unset($params[$key]);
            }
        }

        return [$resource, $params];
    }

    protected function curlInit($resource, array $params, $method)
    {
        $curl = $this->curlCreate();
        $this->curlSetAuth($curl);
        $this->curlSetRequest($curl, $resource, $params, $method);

        return $curl;
    }

    protected function curlCreate()
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_USERAGENT, 'CQM Manhattan API Client');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->config['verify_ssl']);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->config['verify_ssl'] ? 2 : 0);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config['ssl_certs_bundle']);

        return $curl;
    }

    protected function curlSetAuth($curl)
    {
        $signature = Signature::makeHmacSha256($this->config['consumer_secret'], $nonce, $ts);
        $token = implode('#', [$this->config['consumer_key'], $nonce, $ts, $signature]);

        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'X-Auth-Version: hmac_sha256',
            'X-Auth-Token:' . $token,
        ]);
    }

    protected function curlSetRequest($curl, $resource, array $params, $method)
    {
        $url = $this->config['endpoint'] . '/' . $resource;

        /* Params */
        switch (strtoupper($method)) {
            case 'GET':
                $url = $url . '?' . http_build_query($params);
                break;

            case 'POST':
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
                break;

            default:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
                break;
        }

        curl_setopt($curl, CURLOPT_URL, $url);

        return $curl;
    }

    protected function curlExec($curl)
    {
        $response = [
            'body' => curl_exec($curl),
            'curl_errno' => curl_errno($curl),
            'curl_error' => curl_error($curl),
            'http_code' => curl_getinfo($curl, CURLINFO_HTTP_CODE),
        ];

        curl_close($curl);

        return $response;
    }

    protected function createResponse($result)
    {
        if ($result['curl_errno'] !== 0) {
            throw new CurlException($result['curl_error'], $result['curl_errno']);
        }

        $body = json_decode($result['body'], true);
        if (json_last_error()) {
            throw new BadResponseException($result['body'], json_last_error_msg(), json_last_error());
        }

        return new Response($result['body'], $body, $result['http_code']);
    }

}

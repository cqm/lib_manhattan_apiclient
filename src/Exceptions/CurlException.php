<?php

namespace CQM\Libraries\Manhattan\ApiClient\Exceptions;

class CurlException extends ApiClientException
{

    public function __construct($message, $code, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}

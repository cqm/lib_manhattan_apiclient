<?php

namespace CQM\Libraries\Manhattan\ApiClient\Exceptions;

use CQM\Libraries\Manhattan\ApiClient\Response;

class ErrorResponseException extends ApiClientException
{

    /**
     * @var Response
     */
    private $response;

    public function __construct(Response $response, \Throwable $previous = null)
    {
        $message = !empty($response['message']) ? $response['message'] : '';
        $code = !empty($response['code']) ? $response['code'] : 0;
    
        parent::__construct($message, $code, $previous);

        $this->response = $response;
    }

    /**
     * Returns the response
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

}
